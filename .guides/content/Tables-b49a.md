Here are a few examples of table creation using `\begin{array}`.

## Example 1

$
\begin{array}{|c|c|c|}
\hline 
\textbf{col1} & \text{col2} & col3 \\
\hline
r1c1 & r1c2 & r1c3 \\
\hline
r2c1 & r2c2 & r2c3 \\
\hline
\end{array}
$

Note that to get non italicized text, you use \text{}.

This is written as 

```
$
\begin{array}{|c|c|c|}
\hline 
\textbf{col1} & \text{col2} & col3 \\
\hline
r1c1 & r1c2 & r1c3 \\
\hline
r2c1 & r2c2 & r2c3 \\
\hline
\end{array}
$
```

## Example 2
This shows how to avoid vertical lines by removing the `|` character.

$
\begin{array}{c|c|c}
\textbf{col1} & \text{col2} & col3 \\
\hline
r1c1 & r1c2 & r1c3 \\
\hline
r2c1 & r2c2 & r2c3 \\
\end{array}
$

This is written as 

```
$
\begin{array}{c|c|c}
\textbf{col1} & \text{col2} & col3 \\
\hline
r1c1 & r1c2 & r1c3 \\
\hline
r2c1 & r2c2 & r2c3 \\
\end{array}
$
```

There is more you can do with tables, so feel free to Google **mathjax array** or **latex array**.