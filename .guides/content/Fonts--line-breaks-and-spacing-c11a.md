
## Fonts
You can specify different fonts as follows.

Blackboard Bold $\mathbb{COMS 3203}$  `$\mathbb{COMS 3203}$`
Roman $\mathrm{COMS 3203}$  `$\mathbb{COMS 3203}$`
Sans serif $\mathsf{COMS 3203}$  `$\mathsf{COMS 3203}$`
Caligraphic $\mathcal{COMS 3203}$  `$\mathcal{COMS 3203}$`

## Line breaks
Take a look at the following. 

$
y=x^2
a=b^3
$

Written as

```
$
y=x^2
a=b^3
$
```

You can see that even though I have a new line between the expressions, the formula renders on a single line. 

You can solve this using `\\` at the end of the line.

$
y=x^2\\
a=b^3
$

Written as

```
$
y=x^2\\
a=b^3
$
```

## Line height
Let's take the following

$
y=x^3\\
p=q\\
a=\sqrt{r}
$

I can now adjust the vertical spacing as follows

$
y=x^3\\[2ex]
p=q\\[3ex]
a=\sqrt{r}
$

Written as

```
$
y=x^3\\[2ex]
p=q\\[3ex]
a=\sqrt{r}
$
```

## Horizontal spacing
You can also adjust horizontal spacing using `\` followed by a space) or `\space`.

$y=x^2(a+b)$

On the line below, note the extra space after $x^2$.

$y=x^2\ (a+b)$

Written as

`$y=x^2\ (a+b)$`

For more tips on spacing, [click here](https://tex.stackexchange.com/questions/74353/what-commands-are-there-for-horizontal-spacing)
