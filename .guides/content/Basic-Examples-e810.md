The main thing you need to take on board is that you can  mix text and formulae like this.

Let's take a **really simple formula** like $y=x^2$. You can also put thing on multiple line, so

1. Take an expression $y=x$
1. Multiplying $x$ by itself can also be written as $y=x^2$
1. Isn't that fun.

In Codio, the above is written as 

```
Let's take a **really simple formula** like $y=x^2$. You can also put thing on multiple line, so

1. Take an expression $y=x$
1. Multiplying $x$ by itself can also be written as $y=x^2$
1. Isn't that fun.
```

## Example

A few simple examples. Note that the Mathjax expressions should be surrounded by `$...$` when you use them in your answers.

<table>
<tr>
  <td width="300px">$ y=x^2 $ </td>
  <td width=300px>y=x^2</td>
</tr>
<tr>
  <td>$ a=\sqrt{b^2 + c^2} $ </td>
  <td>a=\sqrt{b^2 + c^2} </td>
</tr>
<tr>
  <td>$ y=\frac{1}{x} $ </td>
  <td>y=\frac{1}{x}</td>
</tr>
<tr>
  <td>$\sqrt[3]{k}$ </td>
  <td>\sqrt[3]{k} </td>
</tr>
<tr>
  <td>$\sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2}$ </td>
  <td>\sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2}</td>
</tr>
<tr>
  <td>$\int f(x) dx$ </td>
  <td>\int f(x) dx</td>
</tr>
<tr>
  <td>$\iiint_V f(x)\,dz\,dy\,dx$ </td>
  <td>\iiint_V f(x)\,dz\,dy\,dx</td>
</tr>
<tr>
  <td>$\begin{pmatrix}
     1 & a_1 & a_1^2 & \cdots & a_1^n \\
     1 & a_2 & a_2^2 & \cdots & a_2^n \\
     \vdots  & \vdots& \vdots & \ddots & \vdots \\
     1 & a_m & a_m^2 & \cdots & a_m^n    
     \end{pmatrix}$ </td>
  <td>$
\begin{pmatrix}
     1 & a_1 & a_1^2 & \cdots & a_1^n \\
     1 & a_2 & a_2^2 & \cdots & a_2^n \\
     \vdots  & \vdots& \vdots & \ddots & \vdots \\
     1 & a_m & a_m^2 & \cdots & a_m^n    
     \end{pmatrix}  
  </td>
</tr>

</table>

## More room
Sometimes, you need more room for certain formulae to render nicely. Here's an example.

$ \sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2} $

`$ \sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2} $`

Notice how the upper and lower limits are squeezed onto the same line. You can fix this by using `$$ ... ` instead of just `$ ...$`.

$$ \sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2} $$

`$$ \sum_{i=1}^{n}i=\frac{n\left ( n+1 \right )}{2} $$`

or, you can use the following trick

$
\begin{align}
\sum_{i=1}^n F_{2i-1}=F_1+F_3+F_5+\cdots+F_{2n-1}
\end{align}
$

```
$
\begin{align}
\sum_{i=1}^n F_{2i-1}=F_1+F_3+F_5+\cdots+F_{2n-1}
\end{align}
$
```
