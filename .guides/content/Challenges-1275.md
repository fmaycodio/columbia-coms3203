The following pages contain some challenges. With your new found Latex/Mathjax knowledge, answer each of the questions on the following pages.

You do not need to press the **Submit Answer** button when you move from page to page. However, once you feel you have answered the question then press this button to let your TA know.

