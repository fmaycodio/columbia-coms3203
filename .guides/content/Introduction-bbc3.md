<br>
<table>
<tr>
  <td><img src=".guides/img/logo.png"></td>
  <td><h1>COMS 3203: Discrete Mathematics</h1></td>
</tr>
</table>

---
<br>

This course introduces students in computer science and engineering to the mathematics of computing. Emphasis on applications in computer science and engineering is one of the main strengths of this course. We will cover a broad set of Discrete math topics: 

- **Topic 1 - Logic**: Propositional logic, truth tables, Boolean algebra, theorems, truth, circuits, proofs, inference. 
- **Topic 2 - Proofs**: if-then proof,  contradiction, by cases, counter example. 
- **Topic 3 - Collections (set theory)**: Lists, sets, operations, factorial, cardinality, quantifiers.
- **Topic 4 - Counting \& Relations**:	Relations, equivalence, partitions, binomial coefficients, pascal triangle, inclusion-exclusion, counting multisets
- **Topic 5 - More proofs**:	Smallest counter example, proof by induction
- **Topic 6 - Functions**: Functions, properties (injection, surjection, bijection), composition
- **Topic 7 - Graph Theory**:
		graphs, degrees, trees, planar graphs. 
- **Topic 8 - Probability**:	Sample space, events, random variables, independence, Bayes rule, conditional probability, expectation, variance.
- **Topic 9 - Number Theory**:		Dividing, Greatest common divisor, modular arithmetic, prime numbers, RSA public key encryption.

