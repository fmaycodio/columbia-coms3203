Markdown is a very simple way of formatting your text without needing complex HTML tags or Latex commands.

Below is some sample text and below that is the markdown used to generate it. The things you see below should cover most of your needs.

## Rendered output
Below is some rendered markdown based output.

## An h3 title
Here is some text. You can create **bold words** or *italic ones* very easily.

You can also create standard lists like this

- Line 1
- Line 2
- Line 3

or enumerate ones

1. Line 1
1. Line 2
1. Line 3

## Markdown source
Here is the markdown for the above output.
```
### An h3 title
Here is some text. You can create **bold words** or *italic ones* very easily.

You can also create standard lists like this

- Line 1
- Line 2
- Line 3

or enumerate ones

1. Line 1
1. Line 2
1. Line 3

```

### Full reference
If you want a full markdown reference, [click here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

