Please be aware of these points when completing the assessments on the following pages.

![](.guides/img/instruction.png)

- When you are happy with your answer, you should press the **Submit Answer** button so the TAs can see you have completed it.

- If have already entered an answer but want to come back to it later before submitting, you can move from one page to the next at any time. Your answers will be automatically saved as you type. You can then submit when you are happy with the answer.

- When you have finished all the assessments you should submit your homework by selecting **Mark as completed** from either a) the last page or b) from the **Settings** button in the top bar.

Before you select **Mark as completed** make sure that you have submitted each individual assessment by pressing the **Submit Answer** button beneath each assessment. If it has been submitted, you will see a box telling you so.