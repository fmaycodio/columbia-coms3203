## $\LaTeX$
Latex is a markup language, widely used in science and academia, that can be used to create well formatted documents. One of the main strengths of LATEX is its ability to represent complex mathematical expressions in an elegant and easy way.

## Mathjax
Codio, the system you are using currently, uses a special version of Latex called *Mathjax*. You may find that some Latex commands you find on Google do not work. 

The commands that typically don't work are related to text formatting macros. As long as you are aware of this, you will still be able to enter all formulae that you need to and it is, in fact, easier in Codio than creating proper Latex documents.

[Click here](http://docs.mathjax.org/en/latest/tex.html#supported-latex-commands) for a list of Latex commands supported by Mathjax.

## In action
Here is an example of a typical Mathjax formula.

$$
f(n) =
\begin{cases}
\frac{n}{2},  & \text{if $n$ is even} \\[1.5ex]
3n+1, & \text{if $n$ is odd}
\end{cases}
$$

This is written as

```
$$
f(n) =
\begin{cases}
\frac{n}{2},  & \text{if $n$ is even} \\[1.5ex]
3n+1, & \text{if $n$ is odd}
\end{cases}
$$
```

